#include "freyr/freyr.h"

#include "freyr/common.h"
#include "freyr/gfx_api.h"

#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

static bool running;
static float delta;

void freyr_start() {
	gfx_api_init(OPENGL);
	ecs_init();

	win_init(800, 600, "Freyr");
	con_init(800, 600);
	set_clear_colour(1, 0, 1, 1);

	con_get_version_info();
	freyr_init();
	running = true;
	while(running) {
		clear();
		freyr_update(1);
		freyr_render();
		win_update();

		if(win_is_closed()) {
			running = false;
		}
	}

	freyr_end();
	ecs_end();
	win_end();
	con_end();
	gfx_api_end();
}

void freyr_quit() {
	running = false;
}

float get_delta() {
	return delta;
}

int main(int argc, char **argv) {
	freyr_start();
	return 0;
}
