#include "freyr/ecs.h"

#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <stdio.h>

void ecs_init() {
	// initialize entity component array
	for(u32 i = 0; i < MAX_ENTITIES; i++) {
		bool *is_used = calloc(1, sizeof(bool));
		*is_used = false;
		components[i][COMPONENT_COUNT] = is_used;
	}
}

entity create_entity() {
	entity result = -1;
	for(u32 i = 0; i < MAX_ENTITIES; i++) {
		// find free location for new entity
		bool *is_used = (bool *)components[i][COMPONENT_COUNT];
		if(!*is_used) {
			result = i;
			// set is_used flag [x][COMPONENT_COUNT] to true
			*((bool *)components[i][COMPONENT_COUNT]) = true;
			break;
		}
	}

	assert(result != -1);

	return result;
}

void add_component_to_entity(entity ent, component comp, void *comp_data) {
	switch(comp) {
		case COMP_POSITION: {
			position *pos_data = comp_data;
			position *pos = calloc(1, sizeof(position));
			pos->entity_id = ent;
			pos->x = pos_data->x;
			pos->y = pos_data->y;
			components[ent][comp] = pos;
			} break;
		case COMP_MOVEMENT: {
			movement *mov_data = comp_data;
			movement *mov = calloc(1, sizeof(movement));
			mov->entity_id = ent;
			mov->x = mov_data->x;
			mov->y = mov_data->y;
			components[ent][comp] = mov;
			} break;
		case COMP_PLAYER_INPUT: {
			player_input *inp_data = comp_data;
			player_input *inp = calloc(1, sizeof(player_input));
			inp->entity_id = ent;
			inp->horizontal_axis = inp_data->horizontal_axis;
			inp->vertical_axis = inp_data->vertical_axis;
			components[ent][comp] = inp;
			} break;
		case COMP_SPRITE: {
			sprite *spr_data = comp_data;
			sprite *spr = calloc(1, sizeof(sprite));
			spr->entity_id = ent;
			spr->vao = spr_data->vao;
			spr->ibo = spr_data->ibo;

			components[ent][comp] = spr;
			} break;
		default:
			assert(1 == 0);
			break;
	}
}

void *get_component_from_entity(entity ent, component comp) {
	return components[ent][comp];
}

void destroy_entity(entity ent) {
	for(int i = 0; i < COMPONENT_COUNT; i++) {
		// free checks if the void * suplied is a null pointer, so we do not have to check this ourselves
		free(components[ent][i]);
	}
	*((bool *)components[ent][COMPONENT_COUNT]) = false;
}

void ecs_end() {
	for(u32 i = 0; i < MAX_ENTITIES; i++) {
		free(components[i][COMPONENT_COUNT]);
	}
}

bool entity_is_used(entity ent) {
	bool *is_used = (bool *)components[ent][COMPONENT_COUNT];
	return *is_used;
}
