#include "freyr/gfx_api.h"

#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

void gfx_api_init(GFX_API api) {
	char *err;
	switch(api) {
		case OPENGL:
			handle = dlopen("libmani.so", RTLD_NOW);
			break;
		case VULKAN:
			handle = dlopen("libsol.so", RTLD_NOW);
			break;
		default:
			fprintf(stderr, "you tried to load a non-existent GFX api!");
			exit(1);
			break;
	}
	dlerror();

	// load all the shit here

	win_init = (void(*)(int, int, char *))dlsym(handle, "window_init");
	win_update = (void(*)(void))dlsym(handle, "window_update");
	win_is_closed = (bool(*)(void))dlsym(handle, "window_is_closed");
	win_get_time = (float(*)(void))dlsym(handle, "window_get_time");
	win_end = (void(*)(void))dlsym(handle, "window_end");

	con_init = (void(*)(int, int))dlsym(handle, "context_init");
	con_get_version_info = (void(*)(void))dlsym(handle, "context_get_version_info");
	con_end = (void(*)(void))dlsym(handle, "context_end");

	clear = (void(*)(void))dlsym(handle, "context_clear");
	set_clear_colour = (void(*)(float, float, float, float))dlsym(handle, "context_set_clear_colour");

	// Arrays

	create_vertex_array = (void(*)(vertex_array *))dlsym(handle, "context_create_vertex_array");
	destroy_vertex_array = (void(*)(vertex_array *))dlsym(handle, "context_destroy_vertex_array");
	bind_vertex_array = (void(*)(vertex_array))dlsym(handle, "context_bind_vertex_array");
	unbind_vertex_array = (void(*)(void))dlsym(handle, "context_unbind_vertex_array");

	// Buffers

	create_vertex_buffer = (void(*)(vertex_buffer *, float *, u32))dlsym(handle, "context_create_vertex_buffer");
	create_index_buffer = (void(*)(index_buffer *, u32 *, u32))dlsym(handle, "context_create_index_buffer");

	destroy_vertex_buffer = (void(*)(vertex_buffer *))dlsym(handle, "context_destroy_vertex_buffer");
	destroy_index_buffer = (void(*)(index_buffer *))dlsym(handle, "context_destroy_index_buffer");

	bind_vertex_buffer = (void(*)(vertex_buffer))dlsym(handle, "context_bind_vertex_buffer");
	bind_index_buffer = (void(*)(index_buffer))dlsym(handle, "context_bind_index_buffer");

	unbind_vertex_buffer = (void(*)(void))dlsym(handle, "context_unbind_vertex_buffer");
	unbind_index_buffer = (void(*)(void))dlsym(handle, "context_unbind_index_buffer");

	set_vertex_buffer_layout = (void(*)(vertex_buffer, enum shader_data_type *, u32))dlsym(handle, "context_set_vertex_buffer_layout");

	render_with_indices = (void(*)(vertex_array, index_buffer))dlsym(handle, "context_render_with_indices");

	// Shaders
	
	create_shader_program = (void(*)(shader_program *, const char *, const char *))dlsym(handle, "context_create_shader_program");
	bind_shader_program = (void(*)(shader_program))dlsym(handle, "context_bind_shader_program");
	unbind_shader_program = (void(*)(void))dlsym(handle, "context_unbind_shader_program");

	if((err = dlerror()) != NULL) {
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}
}

void gfx_api_end() {
	dlclose(handle);
}
