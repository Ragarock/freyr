#ifndef GFX_API_H
#define GFX_API_H

#include "gfx_api/window.h"
#include "gfx_api/context.h"

void *handle;

typedef enum {
	OPENGL = 0,
	VULKAN = 1,
} GFX_API;

// Context
void (*con_init)(int, int);
void (*con_get_version_info)(void);
void (*con_end)(void);

void (*clear)(void);
void (*set_clear_colour)(float, float, float, float);

// Arrays
void (*create_vertex_array)(vertex_array *);
void (*destroy_vertex_array)(vertex_array *);
void (*bind_vertex_array)(vertex_array);
void (*unbind_vertex_array)(void);

// Buffers
void (*create_vertex_buffer)(vertex_buffer *, float *, u32);
void (*create_index_buffer)(index_buffer *, u32 *, u32);

void (*destroy_vertex_buffer)(vertex_buffer *);
void (*destroy_index_buffer)(index_buffer *);

void (*bind_vertex_buffer)(vertex_buffer);
void (*bind_index_buffer)(index_buffer);

void (*unbind_vertex_buffer)(void);
void (*unbind_index_buffer)(void);

void (*set_vertex_buffer_layout)(vertex_buffer, enum shader_data_type *, u32);

void (*render_with_indices)(vertex_array, index_buffer);

// Shaders

void (*create_shader_program)(shader_program *, const char *, const char *);
void (*bind_shader_program)(shader_program);
void (*unbind_shader_program)();

// Window
void (*win_init)(int, int, char *);
void (*win_update)(void);
bool (*win_is_closed)(void);
float (*win_get_time)(void);
void (*win_end)(void);

// Loading

void gfx_api_init(GFX_API api);
void gfx_api_end();

#endif//GFX_API_H
