#include "freyr/ecs.h"

void freyr_start();
void freyr_quit();

extern void freyr_init();
extern void freyr_update(float delta);
extern void freyr_render();
extern void freyr_end();

float get_delta();
