#ifndef ECS_H
#define ECS_H

#include "freyr/common.h"

#include "gfx_api/context.h"

#define MAX_ENTITIES 10000

typedef enum {
	COMP_POSITION = 0,
	COMP_MOVEMENT,
	COMP_PLAYER_INPUT,
	COMP_SPRITE,

	COMPONENT_COUNT
} component;

typedef u32 entity;

typedef struct {
	entity entity_id;
	float x;
	float y;
} position;

typedef struct {
	entity entity_id;
	float x;
	float y;
} movement;

typedef struct {
	entity entity_id;
	float vertical_axis;
	float horizontal_axis;
} player_input;

typedef struct {
	entity entity_id;
	vertex_array vao;
	index_buffer ibo;
} sprite;

// slot [x][COMPONENT_COUNT] is reserved for the entity itself
static void *components[MAX_ENTITIES][COMPONENT_COUNT + 1];

void ecs_init();
entity create_entity();
bool entity_is_used(entity ent);
void add_component_to_entity(entity ent, component comp, void *comp_data);
void *get_component_from_entity(entity ent, component comp);
void destroy_entity(entity ent);
void ecs_end();

#endif//ECS_H
