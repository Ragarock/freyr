#ifndef COMMON_H
#define COMMON_H

#include <inttypes.h>
#include <stdbool.h>

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t  i8;

#define null ( (void *) 0)

#endif//COMMON_H
