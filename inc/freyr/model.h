#ifndef MODEL_H
#define MODEL_H

#include <gfx_api/context.h>

typedef struct {
	vertex_array vao;
	vertex_buffer vbo;
	index_buffer ibo;
} model;

#endif//MODEL_H
