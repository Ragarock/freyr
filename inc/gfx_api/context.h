#ifndef CONTEXT_H
#define CONTEXT_H

#include <freyr/common.h>

//#include <cglm/mat4.h>

typedef struct {
	u32 id;
	u32 width;
	u32 height;
} texture;

typedef struct {
	u32 id;
} vertex_array;

typedef struct {
	u32 id;
	u32 count;
} vertex_buffer;

typedef struct {
	u32 id;
	u32 count;
} index_buffer;

enum shader_data_type {
	NONE,
	F1,
	F2,
	F3,
	F4,
	M3,
	M4,
	I1,
	I2,
	I3,
	I4
};

typedef struct {
	u32 id;
} shader_program;

void context_init(int width, int height);
void context_get_version_info();
void context_end();

// Clearing

void context_clear();
void context_set_clear_colour(float r, float g, float b, float a);

// Vertex Array

void context_create_vertex_array(vertex_array *vao);
void context_destroy_vertex_array(vertex_array *vao);
void context_bind_vertex_array(vertex_array vao);
void context_unbind_vertex_array();

// Buffers
void context_create_vertex_buffer(vertex_buffer *vbo, float *data, u32 size);
void context_create_index_buffer(index_buffer *ibo, u32 *data, u32 size);

void context_destroy_vertex_buffer(vertex_buffer *vbo);
void context_destroy_index_buffer(index_buffer *ibo);

void context_bind_vertex_buffer(vertex_buffer vbo);
void context_bind_index_buffer(index_buffer ibo);

void context_unbind_vertex_buffer();
void context_unbind_index_buffer();

void context_set_vertex_buffer_layout(vertex_buffer vbo, enum shader_data_type *layout, u32 count);

void context_render_with_indices(vertex_array vao, index_buffer ibo);

// Shaders

void context_create_shader_program(shader_program *program, const char *vertex_source, const char *fragment_source);
void context_bind_shader_program(shader_program program);
void context_unbind_shader_program();

void context_set_uniform_mat4(char *name, void *matrix);

#endif//CONTEXT_H
