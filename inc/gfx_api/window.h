#ifndef WINDOW_H
#define WINDOW_H

#include "freyr/common.h"

void window_init(int width, int height, char *title);
void window_update(void);
bool window_is_closed(void);
float window_get_time(void);
void window_end(void);

void *get_window_id();

#endif//WINDOW_H
