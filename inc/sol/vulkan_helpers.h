#ifndef VULKAN_HELPERS_H
#define VULKAN_HELPERS_H

#include <freyr/common.h>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#define QUEUE_FAMILY_NULL -1

typedef struct {
	u32 graphics_family;
	u32 present_family;
} queue_family_indices;

typedef struct {
	VkSurfaceCapabilitiesKHR capabilities;
	VkSurfaceFormatKHR *formats;
	u32 format_count;
	VkPresentModeKHR *present_modes;
	u32 present_mode_count;
} swap_chain_support_details;

bool is_queue_family_complete(queue_family_indices indices) {
	return indices.graphics_family != QUEUE_FAMILY_NULL && indices.present_family != QUEUE_FAMILY_NULL;
}

void create_instance(VkInstance *instance);
void destroy_instance(VkInstance instance);

void setup_debug_messenger(VkInstance instance, VkDebugUtilsMessengerEXT *debug_messenger);
void destroy_debug_messenger(VkInstance instance, VkDebugUtilsMessengerEXT debug_messenger);

void pick_physical_device(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice *physical_device);
void destroy_physical_device(VkInstance instance, VkPhysicalDevice physical_device);

void create_logical_device(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice physical_device, VkDevice *device, queue_family_indices *indices);
void destroy_logical_device(VkInstance instance, VkDevice device);

void create_window_surface(VkInstance instance, VkSurfaceKHR *surface);
void destroy_window_surface(VkInstance instance, VkSurfaceKHR surface);

#endif//VULKAN_HELPERS_H
