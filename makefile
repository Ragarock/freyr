CC = tcc
AR = ar

FREYR_SRC = $(wildcard freyr/*.c) $(wildcard freyr/*/*.c)
FREYR_OBJ = $(addprefix obj/freyr/,$(notdir $(FREYR_SRC:.c=.o)))

SOL_SRC = $(wildcard sol/*.c) $(wildcard sol/*/*.c)
SOL_OBJ = $(addprefix obj/sol/,$(notdir $(SOL_SRC:.c=.o)))

MANI_SRC = $(wildcard mani/*.c) $(wildcard mani/*/*.c)
MANI_OBJ = $(addprefix obj/mani/,$(notdir $(MANI_SRC:.c=.o)))

GAME_SRC = $(wildcard game/*.c) $(wildcard game/*/*.c)
GAME_OBJ = $(addprefix obj/game/,$(notdir $(GAME_SRC:.c=.o)))

FREYR_CFLAGS = -I ./inc -std=c99 -Wall -Werror -Wno-unused -O0 -ggdb -fsanitize=address
FREYR_LFLAGS = -shared -O0 -ggdb

SOL_CFLAGS = -I ./inc -std=c99 -Wall -Werror -Wno-unused -O0 -ggdb -fsanitize=address
SOL_LFLAGS = -shared -O0 -ggdb

MANI_CFLAGS = -I ./inc -std=c99 -Wall -Werror -Wno-unused -O0 -ggdb -fsanitize=address
MANI_LFLAGS = -shared -O0 -ggdb

GAME_CFLAGS = -I ./inc -std=c99 -Wall -Werror -Wno-unused -O0 -ggdb -fsanitize=address
GAME_LFLAGS = -O0 -ggdb -Llib -lfreyr

PLATFORM = $(shell uname)

LIB_DIR = ./lib

ifeq ($(findstring Linux,$(PLATFORM)),Linux)
	FREYR_DYNAMIC = libfreyr.so
	FREYR_STATIC  = libfreyr.a
	
	SOL_DYNAMIC = libsol.so
	SOL_STATIC  = libsol.a

	MANI_DYNAMIC = libmani.so
	MANI_STATIC  = libmani.a

	GAME_EXE = game.x86_64

	FREYR_CFLAGS += -fPIC
	FREYR_LFLAGS += -lglfw -lcglm

	SOL_CFLAGS += -fPIC
	SOL_LFLAGS += -lvulkan

	MANI_CFLAGS += -fPIC
	MANI_LFLAGS += -lGL -lGLEW -lcglm

	GAME_LFLAGS += -lglfw -lGLEW
endif

all: $(LIB_DIR)/$(MANI_DYNAMIC) $(LIB_DIR)/$(MANI_STATIC) $(LIB_DIR)/$(SOL_DYNAMIC) $(LIB_DIR)/$(SOL_STATIC) $(LIB_DIR)/$(FREYR_DYNAMIC) $(LIB_DIR)/$(FREYR_STATIC) $(GAME_EXE)

$(LIB_DIR)/$(FREYR_DYNAMIC): $(FREYR_OBJ)
	$(CC) $(FREYR_OBJ) $(FREYR_LFLAGS) -o $@

$(LIB_DIR)/$(FREYR_STATIC): $(FREYR_OBJ)
	$(AR) rcs $@ $(FREYR_OBJ)

obj/freyr/%.o: freyr/%.c | dir
	$(CC) $< -c $(FREYR_CFLAGS) -o $@

obj/freyr/%.o: freyr/*/%.c | dir
	$(CC) $< -c $(FREYR_CFLAGS) -o $@


$(LIB_DIR)/$(SOL_DYNAMIC): $(SOL_OBJ)
	$(CC) $(SOL_OBJ) $(SOL_LFLAGS) -o $@

$(LIB_DIR)/$(SOL_STATIC): $(SOL_OBJ)
	$(AR) rcs $@ $(SOL_OBJ)

obj/sol/%.o: sol/%.c | dir
	$(CC) $< -c $(SOL_CFLAGS) -o $@

obj/sol/%.o: sol/*/%.c | dir
	$(CC) $< -c $(SOL_CFLAGS) -o $@


$(LIB_DIR)/$(MANI_DYNAMIC): $(MANI_OBJ)
	$(CC) $(MANI_OBJ) $(MANI_LFLAGS) -o $@

$(LIB_DIR)/$(MANI_STATIC): $(MANI_OBJ)
	$(AR) rcs $@ $(MANI_OBJ)

obj/mani/%.o: mani/%.c | dir
	$(CC) $< -c $(MANI_CFLAGS) -o $@

obj/mani/%.o: mani/*/%.c | dir
	$(CC) $< -c $(MANI_CFLAGS) -o $@


$(GAME_EXE): $(GAME_OBJ)
	$(CC) $(GAME_OBJ) $(GAME_LFLAGS) -o $@

obj/game/%.o: game/%.c | dir
	$(CC) $< -c $(GAME_CFLAGS) -o $@

obj/game/%.o: game/*/%.c | dir
	$(CC) $< -c $(GAME_CFLAGS) -o $@

dir:
	mkdir -p obj/freyr
	mkdir -p obj/game
	mkdir -p obj/mani
	mkdir -p obj/sol
	mkdir -p lib

freyr.res: freyr.rc
	windres $< -O coff -o $@

clean:
	rm $(FREYR_OBJ) $(SOL_OBJ) $(MANI_OBJ) $(LIB_DIR)/$(SOL_STATIC) $(LIB_DIR)/$(SOL_DYNAMIC) $(LIB_DIR)/$(MANI_STATIC) $(LIB_DIR)/$(MANI_DYNAMIC) $(LIB_DIR)/$(FREYR_STATIC) $(LIB_DIR)/$(FREYR_DYNAMIC) $(GAME_OBJ) $(GAME_EXE)

run:
	env LD_LIBRARY_PATH=$(LIB_DIR) ./$(GAME_EXE)

run-check:
	env LD_LIBRARY_PATH=$(LIB_DIR) valgrind --leak-check=full --show-leak-kinds=all ./$(GAME_EXE)

.PHONY: dir clean
