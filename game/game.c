#include <freyr/freyr.h>
#include <freyr/gfx_api.h>

#include <sys/resource.h>

#include <GL/glew.h>

#include <stdio.h>

#include <cglm/cglm.h>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

//Texture2D player_tex;
vertex_array vao;
vertex_buffer vbo;
index_buffer ibo;
shader_program basic_shader;

void create_player(vertex_array vao, index_buffer ibo) {
	entity player = create_entity();
	position pos_data = {0, 100, 100};
	movement mov_data = {0 ,0 , 0};
	player_input inp_data = {0, 0, 0};
	sprite spr_data = {0, vao, ibo};
	add_component_to_entity(player, COMP_POSITION, &pos_data);
	add_component_to_entity(player, COMP_MOVEMENT, &mov_data);
	add_component_to_entity(player, COMP_PLAYER_INPUT, &inp_data);
	add_component_to_entity(player, COMP_SPRITE, &spr_data);
}

void freyr_init() {
	set_clear_colour(0.1, 0.1, 0.1, 1);
	//player_tex = LoadTexture("res/test.png");

	float vertices[(3 + 4) * 24] = {
		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
	};

	u32 indices[12] = {
		0, 1, 2,
		2, 3, 0,

		4, 5, 6,
		6, 7, 4,
	};

	const char *vertex_source = "\
		#version 120\n \
		attribute vec3 position;\n \
		attribute vec4 colour;\n \
		varying vec4 var_colour;\n \
	 	void main() {\n \
			var_colour = colour;\n \
			gl_Position = vec4(position, 1.0);\n \
		}";

	const char *fragment_source = "\
		#version 120\n \
		varying vec4 var_colour;\n \
	 	void main() {\n \
			gl_FragColor = var_colour;\n \
		}";

	create_shader_program(&basic_shader, vertex_source, fragment_source);

	create_vertex_array(&vao);
	create_vertex_buffer(&vbo, vertices, 7 * 8);
	create_index_buffer(&ibo, indices, 12);
	create_player(vao, ibo);

	enum shader_data_type layout[] = {
		F3,
		F4,
	};

	set_vertex_buffer_layout(vbo, layout, 2);
}

void player_movement_system(entity ent);
void user_input_system(entity ent);
void render_sprite(entity ent);

void freyr_update(float delta) {
	//BeginDrawing();
	//ClearBackground(RAYWHITE);
	//DrawFPS(0, 0);

//	if(IsKeyPressed(KEY_E)) {
//		create_player();
//	}

//	if(IsKeyPressed(KEY_F)) {
//		for(int i = 0; i < MAX_ENTITIES; i++) {
//			if(entity_is_used(i)) {
//				destroy_entity(i);
//			}
//		}
//	}

	for(int i = 0; i < MAX_ENTITIES; i++) {
		if(entity_is_used(i)) {
			user_input_system(i);
			player_movement_system(i);
			render_sprite(i);
		}
	}
	
	//EndDrawing();
}

void freyr_render() {
}

void freyr_end() {
}

void render_sprite(entity ent) {
	position *pos = get_component_from_entity(ent, COMP_POSITION);
	sprite *spr = get_component_from_entity(ent, COMP_SPRITE);

	if(pos == NULL) return;
	if(spr == NULL) return;

	//DrawTexture(spr->texture, pos->x, pos->y, WHITE);

	bind_shader_program(basic_shader);
	render_with_indices(vao, ibo);
}

void player_movement_system(entity ent) {
	player_input *input = get_component_from_entity(ent, COMP_PLAYER_INPUT);
	position *pos = get_component_from_entity(ent, COMP_POSITION);
	if(input == NULL) return;
	if(pos == NULL) return;

	//pos->x += input->horizontal_axis * 64.0f * get_delta(); //TODO: * GetFrameTime();
	//pos->y -= input->vertical_axis * 64.0f * get_delta(); //TODO: * GetFrameTime();
}

void user_input_system(entity ent) {
	// check if user has necessary components
	player_input *input = get_component_from_entity(ent, COMP_PLAYER_INPUT);
	if(input == NULL) return;

	input->vertical_axis = 0;
	input->horizontal_axis = 0;
	//if(IsKeyDown(KEY_S)) input->vertical_axis -= 1;
	//if(IsKeyDown(KEY_W)) input->vertical_axis += 1;
	//if(IsKeyDown(KEY_A)) input->horizontal_axis -= 1;
	//if(IsKeyDown(KEY_D)) input->horizontal_axis += 1;
}
