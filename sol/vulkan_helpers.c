#include <sol/vulkan_helpers.h>

#include <freyr/common.h>

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <gfx_api/window.h>

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
		VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		VkDebugUtilsMessageTypeFlagsEXT message_type,
		const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		void* user_data
		) {
	fprintf(stderr, "Validation Layer: %s\n", callback_data->pMessage);

	return VK_FALSE;
}

const char *validation_layers[] = {
	"VK_LAYER_RENDERDOC_Capture",
	//"VK_LAYER_VALVE_steam_overlay_32",
	"VK_LAYER_VALVE_steam_overlay_64",
	//"VK_LAYER_VALVE_steam_fossilize_32",
	"VK_LAYER_VALVE_steam_fossilize_64",
	"VK_LAYER_LUNARG_api_dump",
	"VK_LAYER_LUNARG_device_simulation",
	"VK_LAYER_LUNARG_monitor",
	"VK_LAYER_LUNARG_screenshot",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_KHRONOS_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_standard_validation",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_GOOGLE_unique_objects"
};

int validation_layer_count = 14;

const char *device_extensions[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

int device_extension_count = 1;

const bool enable_validation_layers = false;

VkResult create_debug_utils_messenger_ext(
		VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *create_info, const VkAllocationCallbacks *allocator, VkDebugUtilsMessengerEXT *debug_messenger
		) {
	PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if(func != NULL) {
		return func(instance, create_info, allocator, debug_messenger);
	} else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

void destroy_debug_utils_messenger_ext(
		VkInstance instance, VkDebugUtilsMessengerEXT debug_messenger, const VkAllocationCallbacks *allocator
		) {
	PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if(func != NULL) {
		return func(instance, debug_messenger, allocator);
	}
}

void populate_debug_create_info(VkDebugUtilsMessengerCreateInfoEXT *create_info) {
	create_info->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	create_info->messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	create_info->messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	create_info->pfnUserCallback = debug_callback;
	create_info->pUserData = NULL; // Optional
}

void setup_debug_messenger(VkInstance instance, VkDebugUtilsMessengerEXT *debug_messenger) {
	if(!enable_validation_layers) return;

	VkDebugUtilsMessengerCreateInfoEXT create_info = {};
	populate_debug_create_info(&create_info);
	//VkDebugUtilsMessengerCreateInfoEXT create_info = {
	//	.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
	//	.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
	//	.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
	//	.pfnUserCallback = debug_callback,
	//	.pUserData = NULL, // Optional
	//};

	if(create_debug_utils_messenger_ext(instance, &create_info, NULL, debug_messenger) != VK_SUCCESS) {
		fprintf(stderr, "Failed to set up debug messenger\n");
	}
}

void destroy_debug_messenger(VkInstance instance, VkDebugUtilsMessengerEXT debug_messenger) {
	if(!enable_validation_layers) return;

	destroy_debug_utils_messenger_ext(instance, debug_messenger, NULL);
}

bool check_validation_layer_support() {
	u32 layer_count;
	vkEnumerateInstanceLayerProperties(&layer_count, NULL);

	VkLayerProperties layer_properties[layer_count];
	vkEnumerateInstanceLayerProperties(&layer_count, layer_properties);

	printf("Available Validation Layers:\n");
	for(u32 i = 0; i < layer_count; i++) {
		printf("\t%d - %s\n", i, layer_properties[i].layerName);
	}

	printf("Validation Layers supported:\n");
	for(u32 i = 0; i < validation_layer_count; i++) {
		bool layer_found = false;
		const char *layer_name = validation_layers[i];

		for(u32 j = 0; j < layer_count; j++) {
			const char *check_name = layer_properties[j].layerName;
			if(strcmp(layer_name, check_name) == 0) {
				printf("\t%s - Available\n", layer_name);
				layer_found = true;
				break;
			}
		}

		if(!layer_found) {
			printf("\t%s - Unavailable\n", layer_name);
			return false;
		}
	}

	return true;
}

void get_required_extensions(const char **extensions, u32 *count) {
	u32 glfw_count;
	const char **glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_count);

	*count = glfw_count;

	if(enable_validation_layers) {
		*count += 1;
	}

	if(extensions == NULL) return;

	if(enable_validation_layers) {
		for(u32 i = 0; i < *count - 1; i ++) {
			extensions[i] = glfw_extensions[i];
		}
		extensions[*count - 1] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
	} else {
		for(u32 i = 0; i < *count; i ++) {
			extensions[i] = glfw_extensions[i];
		}
	}
}

void create_instance(VkInstance *instance) {
	if(enable_validation_layers && !check_validation_layer_support()) {
		fprintf(stderr, "Validation layers requested but not available\n");
	}

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "Freyr",
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "Sol",
		.engineVersion = VK_MAKE_VERSION(1, 0, 0),
		.apiVersion = VK_API_VERSION_1_0,
	};

	u32 extension_count = 0;
	vkEnumerateInstanceExtensionProperties(NULL, &extension_count, NULL);
	VkExtensionProperties extension_properties[extension_count];
	vkEnumerateInstanceExtensionProperties(NULL, &extension_count, extension_properties);

	u32 required_extension_count;
	get_required_extensions(NULL, &required_extension_count);
	const char *required_extensions[required_extension_count];
	get_required_extensions(required_extensions, &required_extension_count);

	printf("Required Extensions:\n");
	for(int i = 0; i < required_extension_count; i ++) {
		printf("\t%d - %s\n", i, required_extensions[i]);
	}

	printf("Available Extensions:\n");
	for(u32 i = 0; i < extension_count; i++) {
		printf("\t%d - %s\n", i, extension_properties[i].extensionName);
	}

	VkInstanceCreateInfo create_info = {};

	VkDebugUtilsMessengerCreateInfoEXT debug_create_info = {};
	if(enable_validation_layers) {
		populate_debug_create_info(&debug_create_info);
		create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		create_info.pApplicationInfo = &app_info;
		create_info.enabledExtensionCount = required_extension_count;
		create_info.ppEnabledExtensionNames = required_extensions;
		create_info.enabledLayerCount = validation_layer_count;
		create_info.ppEnabledLayerNames = validation_layers;
		create_info.pNext = (VkDebugUtilsMessengerCreateInfoEXT *) &debug_create_info;
	} else {
		create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		create_info.pApplicationInfo = &app_info;
		create_info.enabledExtensionCount = required_extension_count;
		create_info.ppEnabledExtensionNames = required_extensions;
		create_info.enabledLayerCount = 0;
		create_info.pNext = NULL;
	}


	VkResult result = vkCreateInstance(&create_info, NULL, instance);
	if(result != VK_SUCCESS) {
		fprintf(stderr, "Failed to create instance!\n");
	}
}

void destroy_instance(VkInstance instance) {
	vkDestroyInstance(instance, NULL);
}

void find_queue_families(VkPhysicalDevice device, VkSurfaceKHR surface, queue_family_indices *indices) {
	u32 queue_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_count, NULL);

	VkQueueFamilyProperties queues[queue_count];
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_count, queues);

	for(u32 i = 0; i < queue_count; i++) {
		if(queues[i].queueCount > 0 && queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices->graphics_family = i;
		}

		VkBool32 present_support = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &present_support);
		if(queues[i].queueCount > 0 && present_support) {
			indices->present_family = i;
		}

		if(is_queue_family_complete(*indices)) {
			break;
		}
	}
}

bool check_device_extension_support(VkPhysicalDevice device) {
	u32 extension_count;
	vkEnumerateDeviceExtensionProperties(device, NULL, &extension_count, NULL);
	VkExtensionProperties available_extensions[extension_count];
	vkEnumerateDeviceExtensionProperties(device, NULL, &extension_count, available_extensions);

	for(u32 i = 0; i < device_extension_count; i++) {
		const char *extension_name = device_extensions[i];
		bool available = false;
		for(u32 j = 0; j < extension_count; j++) {
			char *tmp_name = available_extensions[j].extensionName;
			if(strcmp(extension_name, tmp_name)) {
				available = true;
				break;
			}
		}

		if(!available) {
			return false;
		}
	}

	return true;
}

bool is_device_suitable(VkPhysicalDevice device, VkSurfaceKHR surface) {
	queue_family_indices indices;
	find_queue_families(device, surface, &indices);

	bool extension_supported = check_device_extension_support(device);

	return is_queue_family_complete(indices) && extension_supported;
}

void pick_physical_device(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice *physical_device) {
	*physical_device = VK_NULL_HANDLE;

	u32 device_count = 0;
	vkEnumeratePhysicalDevices(instance, &device_count, NULL);
	if(device_count == 0) {
		fprintf(stderr, "You seem to be missing a Vulkan compatible GPU or APU!\n");
	}

	VkPhysicalDevice devices[device_count];
	vkEnumeratePhysicalDevices(instance, &device_count, devices);

	// Create a better way to figure out which device is most suitable instead of going for the first one
	for(u32 i = 0; i < device_count; i++) {
		if(is_device_suitable(devices[i], surface)) {
			*physical_device = devices[i];
			break;
		}
	}

	if(physical_device == VK_NULL_HANDLE) {
		fprintf(stderr, "Failed to find suitable GPU!");
	}
}

void destroy_physical_device(VkInstance instance, VkPhysicalDevice physical_device) {
}

void create_logical_device(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice physical_device, VkDevice *device, queue_family_indices *indices) {
	find_queue_families(physical_device, surface, indices);

	u32 unique_queue_count = 1;
	// TODO: figure out a way to find how many unique values are in an array
	u32 queues[] = { indices->graphics_family, indices->present_family };

	if(queues[0] != queues[1]) {
		unique_queue_count++;
	}

	VkDeviceQueueCreateInfo queue_create_infos[unique_queue_count];

	float queue_priority = 1.0f;
	for(u32 i = 0; i < unique_queue_count; i++) {
		VkDeviceQueueCreateInfo queue_create_info = {
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.queueFamilyIndex = queues[i],
			.queueCount = 1,
			.pQueuePriorities = &queue_priority,
		};
		queue_create_infos[i] = queue_create_info;
	}

	VkPhysicalDeviceFeatures device_features = {};

	VkDeviceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pQueueCreateInfos = queue_create_infos,
		.queueCreateInfoCount = unique_queue_count,
		.pEnabledFeatures = &device_features,
		.enabledExtensionCount = device_extension_count,
		.ppEnabledExtensionNames = device_extensions,
		.enabledLayerCount = 0,
	};

	if(enable_validation_layers) {
		create_info.enabledLayerCount = validation_layer_count;
		create_info.ppEnabledLayerNames = validation_layers;
	}

	if(vkCreateDevice(physical_device, &create_info, NULL, device) != VK_SUCCESS) {
		fprintf(stderr, "Failed to create logical Device\n");
	}
}

void destroy_logical_device(VkInstance instance, VkDevice device) {
	vkDestroyDevice(device, NULL);
}

void create_window_surface(VkInstance instance, VkSurfaceKHR *surface) {
	if(glfwCreateWindowSurface(instance, get_window_id(), NULL, surface) != VK_SUCCESS) {
		fprintf(stderr, "GLFW failed to create surface\n");
	}
}

void destroy_window_surface(VkInstance instance, VkSurfaceKHR surface) {
	vkDestroySurfaceKHR(instance, surface, NULL);
}

void query_swap_chain_support(VkPhysicalDevice device, swap_chain_support_details *details) {
}
