#include "gfx_api/context.h"
#include "freyr/common.h"

#include <stdio.h>
#include <string.h>

#include <vulkan/vulkan.h>

#include <GLFW/glfw3.h>

#include <sol/vulkan_helpers.h>

VkInstance instance;
VkDebugUtilsMessengerEXT debug_messenger;

VkSurfaceKHR surface;

VkPhysicalDevice physical_device = VK_NULL_HANDLE;
VkDevice device;
queue_family_indices indices;

VkQueue graphics_queue;
VkQueue present_queue;

void context_init(int width, int height) {
	create_instance(&instance);
	setup_debug_messenger(instance, &debug_messenger);

	create_window_surface(instance, &surface);

	pick_physical_device(instance, surface, &physical_device);

	create_logical_device(instance, surface, physical_device, &device, &indices);

	vkGetDeviceQueue(device, indices.graphics_family, 0, &graphics_queue);
	vkGetDeviceQueue(device, indices.present_family, 0, &present_queue);
}

void context_clear() {
}

void context_set_clear_colour(float r, float g, float b, float a) {
}

void context_get_version_info() {
}

void context_end() {
	destroy_logical_device(instance, device);
	destroy_debug_messenger(instance, debug_messenger);
	destroy_window_surface(instance, surface);
	destroy_instance(instance);
}

void context_create_vertex_array(vertex_array *vao) {
}

void context_destroy_vertex_array(vertex_array *vao) {
}

void context_bind_vertex_array(vertex_array vao) {
}

void context_unbind_vertex_array() {
}

void context_create_vertex_buffer(vertex_buffer *vbo, float *data, u32 count) {
}

void context_create_index_buffer(index_buffer *ibo, u32 *data, u32 count) {
}

void context_destroy_vertex_buffer(vertex_buffer *vbo) {
}

void context_destroy_index_buffer(index_buffer *ibo) {
}

void context_bind_vertex_buffer(vertex_buffer vbo) {
}

void context_bind_index_buffer(index_buffer ibo) {
}

void context_unbind_vertex_buffer() {
}

void context_unbind_index_buffer() {
}

void context_create_shader_program(shader_program *program, const char *vertex_source, const char *fragment_source) {
}

void context_bind_shader_program(shader_program program) {
}

void context_unbind_shader_program() {
}

void context_set_vertex_buffer_layout(vertex_buffer vbo, enum shader_data_type *layout, u32 count) {
}

void context_render_with_indices(vertex_array vao, index_buffer ibo) {
}
