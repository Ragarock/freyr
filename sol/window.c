#include "gfx_api/window.h"

#include <GLFW/glfw3.h>

static GLFWwindow *window;

void window_init(int width, int height, char *title) {
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(width, height, title, NULL, NULL);
}

void window_update() {
	glfwSwapBuffers(window);
	glfwPollEvents();
}

bool window_is_closed() {
	return glfwWindowShouldClose(window);
}

void window_end() {
	glfwDestroyWindow(window);
	glfwTerminate();
}

float window_get_time() {
	return glfwGetTime();
}

void *get_window_id() {
	return (void *)window;
}
