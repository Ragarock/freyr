#include "gfx_api/context.h"

#include <GL/glew.h>
#include <stdio.h>

#include <freyr/common.h>

#include <cglm/cglm.h>

shader_program current_program;

void context_init(int width, int height) {
	u32 err = glewInit();
	if(err != GLEW_OK) {
		printf("%s\n", glewGetErrorString(err));
	}
	glViewport(0, 0, width, height);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void context_clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void context_set_clear_colour(float r, float g, float b, float a) {
	glClearColor(r, g, b, a);
}

void context_get_version_info() {
	printf("Vendor: %s\nGL Version: %s\nRenderer: %s\nShader Version: %s\n",glGetString(GL_VENDOR), glGetString(GL_VERSION), glGetString(GL_RENDERER), glGetString(GL_SHADING_LANGUAGE_VERSION));
}

void context_end() {
}

void context_create_vertex_array(vertex_array *vao) {
	glGenVertexArrays(1, &vao->id);
	context_bind_vertex_array(*vao);
}

void context_destroy_vertex_array(vertex_array *vao) {
	glDeleteVertexArrays(1, &vao->id);
}

void context_bind_vertex_array(vertex_array vao) {
	glBindVertexArray(vao.id);
}

void context_unbind_vertex_array() {
	glBindVertexArray(0);
}

void context_create_vertex_buffer(vertex_buffer *vbo, float *data, u32 count) {
	vbo->count = count;
	glGenBuffers(1, &vbo->id);
	context_bind_vertex_buffer(*vbo);

	glBufferData(GL_ARRAY_BUFFER, count * sizeof(float), data, GL_STATIC_DRAW);
}

void context_create_index_buffer(index_buffer *ibo, u32 *data, u32 count) {
	ibo->count = count;
	glGenBuffers(1, &ibo->id);
	context_bind_index_buffer(*ibo);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(u32), data, GL_STATIC_DRAW);
}

void context_destroy_vertex_buffer(vertex_buffer *vbo) {
	glDeleteBuffers(1, &vbo->id);
}

void context_destroy_index_buffer(index_buffer *ibo) {
	glDeleteBuffers(1, &ibo->id);
}

void context_bind_vertex_buffer(vertex_buffer vbo) {
	glBindBuffer(GL_ARRAY_BUFFER, vbo.id);
}

void context_bind_index_buffer(index_buffer ibo) {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo.id);
}

void context_unbind_vertex_buffer() {
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void context_unbind_index_buffer() {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void context_set_uniform_mat4(char *name, void *matrix) {
	mat4 *m_matrix = matrix;
	if(current_program.id != 0)
		glUniformMatrix4fv(glGetUniformLocation(current_program.id, name), 1, GL_FALSE, *m_matrix[0]);
}

void context_create_shader_program(shader_program *program, const char *vertex_source, const char *fragment_source) {
	u32 vs = glCreateShader(GL_VERTEX_SHADER);
	u32 fs = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vs, 1, &vertex_source, NULL);
	glShaderSource(fs, 1, &fragment_source, NULL);

	glCompileShader(vs);
	glCompileShader(fs);

	i32 status;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE) {
		char buffer[512];
		glGetShaderInfoLog(vs, 512, NULL, buffer);
		printf("Vertex Shader:\n%s\n", buffer);
	}

	glGetShaderiv(fs, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE) {
		char buffer[512];
		glGetShaderInfoLog(fs, 512, NULL, buffer);
		printf("Fragment Shader:\n%s\n", buffer);
	}

	program->id = glCreateProgram();

	glAttachShader(program->id, vs);
	glAttachShader(program->id, fs);

	glLinkProgram(program->id);
}

void context_bind_shader_program(shader_program program) {
	glUseProgram(program.id);
	current_program.id = program.id;
}

void context_unbind_shader_program() {
	glUseProgram(0);
}

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

u32 get_data_type_size(enum shader_data_type type) {
	switch(type) {
		case NONE: return 0; break;
		case F1: return sizeof(float)    ; break;
		case F2: return sizeof(float) * 2; break;
		case F3: return sizeof(float) * 3; break;
		case F4: return sizeof(float) * 4; break;

		case I1: return sizeof(int)    ; break;
		case I2: return sizeof(int) * 2; break;
		case I3: return sizeof(int) * 3; break;
		case I4: return sizeof(int) * 4; break;

		case M3: return sizeof(float) * 3 * 3; break;
		case M4: return sizeof(float) * 4 * 4; break;
	}

	fprintf(stderr, "unknown shader data type!\n");
	return 0;
}

u32 get_data_type_element_count(enum shader_data_type type) {
	switch(type) {
		case NONE: return 0; break;
		case F1: return 1; break;
		case F2: return 2; break;
		case F3: return 3; break;
		case F4: return 4; break;

		case I1: return 1; break;
		case I2: return 2; break;
		case I3: return 3; break;
		case I4: return 4; break;

		case M3: return 3 * 3; break;
		case M4: return 4 * 4; break;
	}

	fprintf(stderr, "unknown shader data type!\n");
	return 0;
}

GLenum data_type_to_opengl_type(enum shader_data_type type) {
	switch(type) {
		case NONE: return 0; break;
		case F1: return GL_FLOAT; break;
		case F2: return GL_FLOAT; break;
		case F3: return GL_FLOAT; break;
		case F4: return GL_FLOAT; break;

		case I1: return GL_INT; break;
		case I2: return GL_INT; break;
		case I3: return GL_INT; break;
		case I4: return GL_INT; break;

		case M3: return GL_FLOAT; break;
		case M4: return GL_FLOAT; break;
	}

	fprintf(stderr, "unknown shader data type!\n");
	return 0;
}

void context_set_vertex_buffer_layout(vertex_buffer vbo, enum shader_data_type *layout, u32 count) {
	context_bind_vertex_buffer(vbo);

	u32 current_offset = 0;
	u32 total_size = 0;

	for(u32 i = 0; i < count; i++) {
		total_size += get_data_type_size(layout[i]);
	}
	
	for(u32 i = 0; i < count; i++) {
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, get_data_type_element_count(layout[i]), data_type_to_opengl_type(layout[i]), false, total_size, BUFFER_OFFSET(current_offset));
		current_offset = get_data_type_size(layout[i]);
	}
}

void context_render_with_indices(vertex_array vao, index_buffer ibo) {
	context_bind_vertex_array(vao);
	glDrawElements(GL_TRIANGLES, ibo.count, GL_UNSIGNED_INT, NULL);
}
